import urllib.request as urlRequest
import os

import boto3
import botocore
from boto3.s3.transfer import S3Transfer
from pytube import YouTube
import re


AWS_VIDEO_BUCKET_NAME="video-crawler-repo"
aws_video_access_key_id="AKIAZQ7XTVCI3Z66UAPN"
aws_video_secret_access_key="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"
AWS_VIDEO_KEY="/"

videoCredentials = {
            "aws_access_key_id": aws_video_access_key_id,
            "aws_secret_access_key": aws_video_secret_access_key
        }

class FileDownloader:

    currentPath = os.path.dirname(os.path.realpath(__file__))
    parentPath = os.path.abspath(os.path.join(currentPath, os.pardir))
    temp_dir = parentPath + '/Downloads/'
    print(parentPath)
    
    def __init__(self):
        pass
        
    
    def download_File(self, url):
        # prepare save dir and filename        
        if not os.path.exists(self.temp_dir):
            os.mkdir(self.temp_dir)
        file_name = url.split('/')[-1]
        if "amazon-crawl-data" in url:
            self.downloadS3(file_name, self.temp_dir + file_name)
            return  file_name
        else:
            try:
                print("simple url")
                urlRequest.urlretrieve(url, self.temp_dir + file_name)
                return  file_name
            except Exception as e:
                raise Exception('download error with exception' + str(e))

    
    def downloadS3(self, file_name,file_dir_name):
        credentials = { 
            'aws_access_key_id': aws_video_access_key_id,
            'aws_secret_access_key': aws_video_secret_access_key
        }
        s3 = boto3.resource('s3', **credentials)
        BUCKET_NAME = AWS_VIDEO_BUCKET_NAME
        KEY = None
        try:
            s3.Bucket(BUCKET_NAME).download_file( KEY+ file_name, file_dir_name )
            return file_name
        except botocore.exceptions.ClientError as e:
            if e.response['Error']['Code'] == "404":
                print("The object does not exist.")
            else:
                raise

            
    
    def upload_To_S3(self, file_name, bucket, key, credentials ):
        try:
            if key != None:
                key_file_name = key + file_name
            else:
                key_file_name = file_name.split("/")[6]
            client = boto3.client('s3', **credentials)
            transfer = S3Transfer(client)
            if ".mp4" in file_name:
                print(key_file_name)
                transfer.upload_file( file_name, bucket, key_file_name, 
                    extra_args={'CacheControl':'max-age=31536000','ContentType':'video/mp4',})
                return "https://d3i3lk5iax7dja.cloudfront.net/" + key_file_name
            transfer.upload_file( file_name, bucket, key_file_name)
            file_url = '%s/%s/%s' % (client.meta.endpoint_url, bucket, key_file_name)
            return file_url
        except Exception as e:
            print(e)
    
    def check_if_exists_s3(self, file_name, bucket, key, credentials):
        s3 = boto3.resource('s3', **credentials)
        BUCKET_NAME = bucket
        key = key + file_name
        objs = list(s3.Bucket(BUCKET_NAME).objects.filter(Prefix=key))
        if len(objs) > 0 and objs[0].key == key:
            return True
        else:
            return False
    
    def download_youtube_link(self,link,resolution):
        try:
            you_tube = YouTube(link)
            stream = you_tube.streams.filter(res=resolution,mime_type='video/mp4').first()
        except Exception as e:
            raise Exception('Download error with excpetion as '+ str(e))


        fileName = stream.download(self.temp_dir,filename=link.split("=")[1]+".mp4")
        return fileName

    def strip_special_char(self,title):
        title = re.sub('\W+','',title)
        return title
    
    def process(self, link):
        url=""
        try:
            print("Downloading from youtube")
            file_name = self.download_youtube_link(link,"720p")
            print("Uploading to S3")
            url = self.upload_To_S3(file_name, AWS_VIDEO_BUCKET_NAME,None , videoCredentials )
            print("Removing file")
            os.remove(file_name)
        except Exception as ex:
            print(ex)
        return url

    def upload(self, file_name ):
  
        
        url = self.upload_To_S3(file_name, AWS_VIDEO_BUCKET_NAME,None , videoCredentials )

      
        return url
